###Programming Assignment 1 ###

*  For this assignment, students will be setting up their BitBucket accounts and pushing the Rails application to a repository on BitBucket. (For instructions on how to use Git to accomplish this, please refer to the posted instructions) For help and more in depth instructions on this assignment, please refer to the Module 2 Lecture 3 Video. 

* This corresponds to iteration 1 of the blog app in the lecture videos. Please make sure to use the same names used in the videos for all Rails variables, classes, etc.. The grading scheme depends on you following the exact instructions provided in the videos. 

* Create a BitBucket account You will need to create a BitBucket account at bitbucket.org. 

* Create the application using Ruby on Rails Create a rails project, using the command. This project should be named . You will need to scaffold tables for posts and comments. The posts table should have the following two columns: of type string and  of type text. The comments table will also have two columns as follows: of type integer and  of type text. 

* Push the application to BitBucket Create a repository on BitBucket (be sure this repository is public). Then push your Rails project to this repository. Refer to the git instructions posted for help with this step. Finally, follow the instructions on the assignment page to submit your Bitbucket URL to the automated grader queue.